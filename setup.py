# -*- coding: utf8-*-
from setuptools import setup
import sys

ver_dic = {}
version_file = open("version.py")
try:
    version_file_contents = version_file.read()
finally:
    version_file.close()

exec(compile(version_file_contents, "version.py", 'exec'), ver_dic)

build_exe_options = {
    "packages": ["os"],
    # "excludes": ["tkinter"],
    "compressed": True
}

base = None
options = None
executables = None

setup(
    name="convert_4d",
    version=ver_dic["VERSION_TEXT"],
    description="Simple 4D->3D and 3D->4D-converter for NifTi-files",
    author="Hannes Wahl",
    author_email="hannes.wahl@uniklinikum-dresden.de",
    setup_requires=['setuptools>=18.0', ],
    install_requires=['numpy', 'nibabel', 'tqdm'],
    packages=[''],
    entry_points={
        'console_scripts': ['4dconverter=convert_4d:main'],
    },
    options=options,
    executables=executables
)
