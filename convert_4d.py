#!/usr/bin/env python3

import os
import nibabel as nib
from glob import glob
from tqdm import tqdm

class Converter(object):
    """
        Convert stack of 3D-nifti files to a single 4D-nifti-file or vice versa
    """

    def __init__(self, input, direction, output_folder=None, output_basename="vol", gzip=False, mean=False ):
        """

        :param kwds: keywords include direction, input, outputfolder,  output-basename
        """
        if direction == "423" or direction == "324":
            self.direction = direction
        else:
            raise IOError("#4: Conversion direction was set to unknown direction, set it to 423 or 324")
        self.input = []

        # check if we got a file list
        if isinstance(input, list):
            self.input = [os.path.abspath(inp) for inp in input if ".nii" in inp]

        # if not a list, but a directory, get .nii-files from directory
        elif os.path.isdir(input):
            self.input = glob(os.path.join(input,'*.nii*'))
        # else check if it is a single .nii-file and we do a 4D->3D conversion, check dimensions afterwards
        elif os.path.exists(input) and self.direction == "423":
            if nib.load(input).shape < 4:
                raise IOError("#1: Input is not a 4D-file, but mode was set to 4D->3D-conversion!")
            else:
                self.input.append(input)
        # else throw error, because unknown input
        else:
            raise IOError("#2: Unknown input -- abort...")

        # double-check if input is set
        if not self.input:
            raise IOError("#3: Something's wrong with the input -- abort...")

        if output_folder:
            self.out_folder = os.path.abspath(output_folder)
            try: os.makedirs(self.out_folder)
            except: pass
        else:
            self.out_folder = os.path.abspath(os.path.dirname(self.input[0]))

        self.out_basename = output_basename

        if gzip:
            self.suffix = ".nii.gz"
        else:
            self.suffix = ".nii"

        self.mean = mean

    def __call__(self):
        self.run()

    def run(self):
        if self.direction == "423":
            self.convert423()
        elif self.direction == "324":
            self.convert324()

        print("Finished.")

    def convert423(self):
        """
        split 4D-input into separate 3D-files, all .nii, either .gz or not, depending on initialisation
        """
        img = nib.load(self.input[0])
        data = img.get_data()
        outpath = os.path.join(self.out_folder, self.out_basename)
        print("saving data to: " + outpath)

        for i in tqdm(range(0, img.shape[3])):
           nib.save(nib.Nifti1Image(data[:, :, :, i],
                                    header=img.header,
                                    affine=img.affine),
                    '%s%04i%s' % (outpath,
                                  i,
                                  self.suffix))

        if self.mean:
            outpath = os.path.join(self.out_folder, 'mean'+os.path.basename(self.input[0]))
            nib.save(nib.Nifti1Image(data.mean(3),
                                     header=img.header,
                                     affine=img.affine),
                     '%s%s' % (outpath, self.suffix))

    def convert324(self):
        """
        merge list of 3D-input into 4D-files
        """
        import numpy as np

        tmp = nib.load(self.input[0])
        header = tmp.header
        affine = tmp.affine
        data = np.zeros(list(tmp.get_data().shape) + [len(self.input)], dtype=header.get_data_dtype())
        for i in tqdm(range(0, len(self.input))):
            data[:,:,:,i] = nib.load(self.input[i]).get_data()

        out_nifti = nib.Nifti1Image(data,
                                    header=header,
                                    affine=affine)
        nib.save(out_nifti, os.path.join(self.out_folder,self.out_basename+self.suffix))
        if self.mean:
            mean_nifti = nib.Nifti1Image(data.mean(3),
                                    header=header,
                                    affine=affine)
            nib.save(mean_nifti,os.path.join(self.out_folder,'mean'+self.out_basename+self.suffix))

def main():
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-i', '--input',nargs="*",required=True, help="inputfolder or input files")
    parser.add_argument('-ofolder', help="outputfolder, if not set, output==input")
    parser.add_argument('-oname', help="output basename, default: vol")
    parser.add_argument('-d', '--direction', choices = ['324', '423'], required=True, help="Set direction of conversion (3D->4D = 324; 4D->3D=423)")
    parser.add_argument('-g','--gzip', action="store_true", help="save gzipped")
    parser.add_argument('-m','--mean', action="store_true", help="generate average/mean image of 4D-file")

    args = parser.parse_args()

    convert = Converter(input=args.input,
                        output_folder = args.ofolder,
                        output_basename = args.oname,
                        direction = args.direction,
                        gzip=args.gzip,
                        mean = args.mean)
    convert.run()

if __name__ == "__main__":
    main()